package com.nehaangural.intent_example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button button1,button2,button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1=findViewById(R.id.buttonExplicit);
        button2=findViewById(R.id.buttonImplicit);
        button3=findViewById(R.id.buttonSendEmail);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Main2Activity.class);
                intent.putExtra("key","value");
                intent.putExtra("a2","1");
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Uri u=Uri.parse("tel:9877654321");
                Intent i=new Intent(Intent.ACTION_DIAL);
                startActivity(i);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Intent.ACTION_SEND);
                /* i.putExtra(Intent.EXTRA_EMAIL,new String[]{""});
                i.putExtra(Intent.EXTRA_SUBJECT,"hELLOO");
                I.putExtra(Intent.EXTRA_TEXT,"hiiiii");

                 */
                i.setType("text/plain");
                startActivity(i);
            }



        });

    }
}

