package com.nehaangural.intent_example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent=getIntent();
        String val=intent.getStringExtra("key");
        int value=intent.getIntExtra("a2",0);
        Toast.makeText(this, val+value, Toast.LENGTH_SHORT).show();
    }
}